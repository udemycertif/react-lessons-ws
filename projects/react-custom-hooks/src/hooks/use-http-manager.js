import {useState, useCallback} from 'react';
import FIREBASE_URL from '../constant/url.constant';

function useHttpManager() {
  const [isLoading, setIsLoading] = useState(false)
  const [error, setError] = useState(null)

  const handleReq = useCallback(async (httpMethod, taskText) => {
      if(httpMethod !== 'GET' && httpMethod !== 'POST') {
          throw new Error(`Http request method ${httpMethod} not implemented`);
      }
      return httpMethod === 'GET' ? await fetch(
        `${FIREBASE_URL}/tasks.json`
      ) : await fetch(
        `${FIREBASE_URL}/tasks.json`,
        {
          method: 'POST',
          body: JSON.stringify({ text: taskText }),
          headers: {
            'Content-Type': 'application/json',
          },
        }
      );
  }, [])

  const formatFetchedData = (data) => {
    const loadedTasks = [];

    for (const taskKey in data) {
      loadedTasks.push({ id: taskKey, text: data[taskKey].text });
    }

    return loadedTasks
  }

  const formatPostedData = (data, taskText) => {
    const generatedId = data.name; // firebase-specific => "name" contains generated id
    const createdTask = { id: generatedId, text: taskText };

    return createdTask
  }

  const httpRequestTasks = useCallback(async (httpMethod, taskText) => {
    setIsLoading(true);
    setError(null);
    try {
      const response = await handleReq(httpMethod, taskText);

      if (!response.ok) {
        throw new Error('Request failed!');
      }

      const data = await response.json();

      if(httpMethod !== 'GET' && httpMethod !== 'POST') {
        throw new Error(`Http request method ${httpMethod} not implemented`);
      }
      
      setIsLoading(false);
      return httpMethod === 'GET' ? formatFetchedData(data) : formatPostedData(data, taskText);
    } catch (err) {
      setError(err.message || 'Something went wrong!');
    }
  }, [handleReq])

  return [isLoading, error, httpRequestTasks];
}

export default useHttpManager
