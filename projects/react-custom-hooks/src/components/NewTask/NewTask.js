import Section from '../UI/Section';
import TaskForm from './TaskForm';
import useHttpManager from '../../hooks/use-http-manager';

const NewTask = (props) => {
  const [isLoading, error, sendRequest] = useHttpManager()

  const enterTaskHandler = async (taskText) => {

    console.log('enterTaskHandler ??? ')

    props.onAddTask(await sendRequest('POST', taskText))
  }

  return (
    <Section>
      <TaskForm onEnterTask={enterTaskHandler} loading={isLoading} />
      {error && <p>{error}</p>}
    </Section>
  );
};

export default NewTask;
