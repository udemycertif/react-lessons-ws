import React, { useEffect, useState } from 'react';

import Tasks from './components/Tasks/Tasks';
import NewTask from './components/NewTask/NewTask';
import useHttpManager from './hooks/use-http-manager';

function App() {
  const [tasks, setTasks] = useState([]);
  const [isLoading, error, sendRequest] = useHttpManager()

  useEffect(() => {
    async function fetchData() {
      setTasks(await sendRequest('GET')); 
    }
    fetchData()
  }, [sendRequest]);

  const taskAddHandler = (task) => {
    setTasks((prevTasks) => prevTasks.concat(task));
  };

  return (
    <React.Fragment>
      <NewTask onAddTask={taskAddHandler} />
      <Tasks
        items={tasks}
        loading={isLoading}
        error={error}
        onFetch={sendRequest}
      />
    </React.Fragment>
  );
}

export default App;
