import { useParams } from 'react-router-dom'

const ProductDetail = () => {
  const params = useParams()
  return (
    <section>
      <h1> Hey ! I'm product detail </h1>
      <h2> Product id: {params.id} </h2>
    </section>
  )
}

export default ProductDetail
