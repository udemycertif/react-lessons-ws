import { Route } from "react-router"

const Welcome = () => {
  return (
    <section>
      <h1> The Welcome Page </h1>
      <Route path='/welcome/new-u'>
          <p> Welcome new user </p>
      </Route>
      <Route path='/welcome/co-u'>
          <p> Welcome user Jeanne Doe </p>
      </Route>
    </section>
  )
}

export default Welcome
