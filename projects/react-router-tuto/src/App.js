import { Route, Switch, Redirect } from 'react-router-dom'
import MainHeader from './components/MainHeader'
import Products from './views/Products'
import ProductDetail from './views/ProductDetail'
import Welcome from './views/Welcome'

function App() {
  return (
    <div>
      <MainHeader />
      <main>
        <h2>Let's get started!</h2>
        <Switch>
          <Route path="/" exact>
            <Redirect to="/welcome"></Redirect>
          </Route>
          <Route path="/welcome">
            <Welcome />
          </Route>
          <Route path="/products" exact>
            <Products />
          </Route>
          <Route path="/products/:id">
            <ProductDetail />
          </Route>
        </Switch>
      </main>
    </div>
  )
}

export default App
