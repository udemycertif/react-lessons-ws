import { useSelector, useDispatch } from 'react-redux'
import classes from './Counter.module.css'

const Counter = () => {
  const counter = useSelector((state) => state.counter)
  const dispatch = useDispatch()
  const toggleCounterHandler = () => {}
  const incrementCounter = () => {
    dispatch({ type: 'increment' })
  }

  const decrementCounter = () => {
    dispatch({ type: 'decrement' })
  }

  return (
    <main className={classes.counter}>
      <h1>Redux Counter</h1>
      <div className={classes.value}>-- COUNTER VALUE --</div>
      <div className={classes.value}>{counter}</div>
      <div>
        <button onClick={incrementCounter}>Increment Counter</button>
        <button onClick={decrementCounter}>Decrement Counter</button>
      </div>
      <button onClick={toggleCounterHandler}>Toggle Counter</button>
    </main>
  )
}

export default Counter
