const counterInitialState = { counter: 0 }

const counterReducer = (state = counterInitialState, action) => {
  switch (action.type) {
    case 'increment':
      return { ...state, counter: state.counter + 1 }
    case 'decrement':
      return { ...state, counter: state.counter - 1 }
    default:
      return state
  }
}

export default counterReducer
